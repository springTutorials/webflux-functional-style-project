package com.vk.example.functionalStyle.webfluxfunctionalstyleproject;

import com.vk.example.functionalStyle.webfluxfunctionalstyleproject.handler.CarHandler;
import com.vk.example.functionalStyle.webfluxfunctionalstyleproject.model.Car;
import com.vk.example.functionalStyle.webfluxfunctionalstyleproject.repository.CarRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;

import static org.springframework.web.reactive.function.server.RequestPredicates.DELETE;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.PUT;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@SpringBootApplication
public class WebfluxFunctionalStyleProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebfluxFunctionalStyleProjectApplication.class, args);
}

	@Bean
	CommandLineRunner init(CarRepository carRepository) {
		return args -> {
			Flux<Car> carFlux = Flux.just(
					new Car("VW", "Golf", "3000e"),
					new Car("Audi", "A8", "5000e"),
					new Car("Toyota", "Rav4", "8000e"))
					.flatMap(carRepository::save);

			carFlux.thenMany(carRepository.findAll()).subscribe(a -> System.out.println(a.getModel()));
		};
	}

	@Bean
	RouterFunction<ServerResponse> routes(CarHandler carHandler) {
		return route(GET("/cars").and(accept(MediaType.APPLICATION_JSON_UTF8)), carHandler::getAllCars)
				.andRoute(GET("/cars/{id}").and(accept(MediaType.APPLICATION_JSON_UTF8)), carHandler::getCarById)
				.andRoute(DELETE("/cars/{id}").and(accept(MediaType.APPLICATION_JSON_UTF8)), carHandler::deleteCarById)
				.andRoute(POST("/car").and(accept(MediaType.APPLICATION_JSON_UTF8)), carHandler::createCar)
				.andRoute(PUT("/car/{id}").and(accept(MediaType.APPLICATION_JSON_UTF8)), carHandler::updateCar);
	}
}
