package com.vk.example.functionalStyle.webfluxfunctionalstyleproject.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Car {

    @Id
    private String id;
    private String company;
    private String model;
    private String price;

    public Car() {}

    public Car(String company, String model, String price) {
        this.company = company;
        this.model = model;
        this.price = price;
    }

    public Car(String id, String company, String model, String price) {
        this(company, model, price);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
