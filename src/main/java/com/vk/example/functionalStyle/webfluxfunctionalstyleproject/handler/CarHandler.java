package com.vk.example.functionalStyle.webfluxfunctionalstyleproject.handler;

import com.vk.example.functionalStyle.webfluxfunctionalstyleproject.model.Car;
import com.vk.example.functionalStyle.webfluxfunctionalstyleproject.repository.CarRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class CarHandler
{
	private final CarRepository carRepository;
	private final static Mono<ServerResponse> NOT_FOUND = ServerResponse.notFound().build();

	public CarHandler(CarRepository carRepository) {
		this.carRepository = carRepository;
	}

	public Mono<ServerResponse> getAllCars(ServerRequest request) {
		Flux<Car> carFlux = carRepository.findAll();
		return ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.body(carFlux, Car.class)
				.switchIfEmpty(NOT_FOUND);
	}

	public Mono<ServerResponse> getCarById(ServerRequest request) {
		String id = request.pathVariable("id");
		Mono<Car> carMono = carRepository.findById(id);
		return ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.body(carMono, Car.class)
				.switchIfEmpty(NOT_FOUND);
	}

	public Mono<ServerResponse> deleteCarById(ServerRequest request) {
		String id = request.pathVariable("id");
		Mono<Car> monoCar = carRepository.findById(id);
		return monoCar.flatMap(car -> ServerResponse.ok()
				.build(carRepository.delete(car)))
				.switchIfEmpty(NOT_FOUND);
	}

	public Mono<ServerResponse> createCar(ServerRequest request) {
		Mono<Car> carMono = request.bodyToMono(Car.class);
		return carMono.flatMap(car -> ServerResponse.status(HttpStatus.CREATED)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.body(carRepository.save(car), Car.class));
	}

	public Mono<ServerResponse> updateCar(ServerRequest request) {
		Mono<Car> newCar = request.bodyToMono(Car.class);
		String id = request.pathVariable("id");
		Mono<Car> existingCarMono = carRepository.findById(id);
		return newCar.zipWith(existingCarMono, (car, oldCar) -> new Car(oldCar.getId(), car.getPrice(), car.getModel(), car.getPrice()))
				.flatMap(car -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.body(carRepository.save(car), Car.class))
				.switchIfEmpty(NOT_FOUND);
	}
}
