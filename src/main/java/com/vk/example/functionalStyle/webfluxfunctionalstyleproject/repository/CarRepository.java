package com.vk.example.functionalStyle.webfluxfunctionalstyleproject.repository;

import com.vk.example.functionalStyle.webfluxfunctionalstyleproject.model.Car;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface CarRepository extends ReactiveMongoRepository<Car, String> {
}
